
#include <list>
#include <string>
#include <vector>

using namespace std;

class Vertex;

class Graph
{
public:

    vector<Vertex*> Vertexes;

    void AddVertex(Vertex* Vertex);
    

};

void Graph::AddVertex(Vertex* Vertex)
{
    Vertexes.push_back(Vertex);
}

class Vertex
{
public:
    int Index;

    Vertex(int Index, Graph* CurrentGraph);

    void CreateEdge(Vertex* Vertex);

    vector<Vertex*> Edges;
    Graph* CurrentGraph; 
    
};

Vertex::Vertex(int Index, Graph* CurrentGraph)
{
    this->Index = Index;
    this->CurrentGraph = CurrentGraph;
    CurrentGraph->AddVertex(this);
}

void Vertex::CreateEdge(Vertex* Vertex)
{
    Edges.push_back(Vertex);
    
    if(Vertex != this)
    Vertex->Edges.push_back(this); 
}


int main(int argc, char* argv[])
{
    Graph* GraphRef = new Graph;
    
    Vertex* One = new Vertex(1, GraphRef);
    Vertex* Two = new Vertex(2, GraphRef);
    Vertex* Three = new Vertex(3, GraphRef);
    Vertex* Four = new Vertex(4, GraphRef);
    Vertex* Five = new Vertex(5, GraphRef);
    
    One->CreateEdge(Two);
    Two->CreateEdge(Four);
    Two->CreateEdge(Three);
    Five->CreateEdge(Five);

    
    return 0;
}
